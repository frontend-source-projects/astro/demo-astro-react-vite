import React, { useState } from 'react'
import { Dialog } from 'primereact/dialog'
import { Button } from 'primereact/button'
import 'primereact/resources/themes/bootstrap4-dark-blue/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import './DialogDemo.css'

function BasicDialog() {
    const [dialogVisible, setDialogVisible] = useState(false)

    const showDialog = () => {
        console.log(dialogVisible)
        setDialogVisible(true)
    }

    const hideDialog = () => {
        console.log(dialogVisible)
        setDialogVisible(false)
    }

    return (
        <>
            <Button label="Mostrar Diálogo" icon="pi pi-external-link" onClick={() => showDialog()} />
            <Dialog header="Mensaje" visible={dialogVisible} style={{ width: '50vw' }} onHide={hideDialog}>
                <div slot='top'>
                    ¡Hola mundo! Primera web con Astro + React + PrimeReact
                </div>
                <div slot="footer" class="dialog-footer">
                    <div className="spacer"></div>
                    <Button label="Cerrar" icon="pi pi-times" onClick={() => hideDialog()} className="p-button-text" />
                    {/* <Button label="Yes" icon="pi pi-check" onClick={() => showDialog()} autoFocus /> */}
                </div>
            </Dialog>
        </>
    )
}

export default BasicDialog